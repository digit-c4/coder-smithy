# Coder-Smithy

<div style="text-align: center;">
<img src="doc/images/coder.svg" alt="Login" width="300" height="auto" style="vertical-align: middle;">
<img src="doc/images/smithy.svg" alt="Login" width="300" height="auto" style="vertical-align: middle;">
</div>

[Coder](https://coder.com) is an open source tool allowing to dynamically create development workspaces, pre-configured with the dependencies needed to work on a specific project.

This initial implementation is focussed on providing an environment allowing to work with Smithy

# User Manual

## How to connect

While not mandatory, it is advised to use Chrome as browser, as it provides the best experience when using the actual development environment.

When connecting to the instance, you have to connect with your email address and the password you received separately.

<img src="doc/images/login.png" alt="Login" width="300" height="auto">

Once logged-in, you will see the dashboard where your created workspaces will be shown.  Initially, it will be empty.

<img src="doc/images/main.png" alt="Main page" width="800" height="auto">

## Change password

If you connect for the first time, please change you password by going to you account:

<img src="doc/images/account.png" alt="Account" width="250" height="auto">
<img src="doc/images/change_password.png" alt="Change Password" width="550" height="auto">

## Use workspaces

A Workspace is a specific environment that will hold your project's files together with the tools needed to work on it.
The content of this environment (tools, etc.) is based on a template that ensure the presence of all the dependencies.

Initially there will be only one template available, providing the Smithy-ready environment.

You can have several environments created next to each other, to work on different projects, each of them potentially based on different template.
Each of these workspace will allow you to pull and push from a repository and work on the files.

If deleting a workspace, the data contained in it will be lost, so be sure to push to a repo any modification that needs to be saved.

A workspace can also be "stopped", it is only a way to preserve compute resource.  Data is not lost, you just need to restart the workspace to continue working on this specific project.
To save resources, you workspaces will be stopped automatically after a few hours of inactivity.

### Create new workspace

Click "Create workspace" and select the template you want to use, based on the type of project you want to work on (today, only Smithy).

<img src="doc/images/create_workspace.png" alt="Create Workspace" width="300" height="auto">

Give it a name (meaningful for you) and click "Create"
You then get you the page with the details on the newly created workspace, showing the creation activities.

After a few minutes/seconds, the workspace will become available.
To start working you can select "Code-Server".  This will launch in a new browser window a fully web-based visual-studio code in which you will be able to pull your project and work on it.

Going back to the dashboard, you will see all your existing workspaces.

<img src="doc/images/dashboard.png" alt="Dashboard" width="700" height="auto">

### Delete workspace

To delete a workspace, in the workspace tab select the 3 points on top-right and select Delete.

<img src="doc/images/delete.png" alt="Delete" width="250" height="auto">

You will have to retype the full name of the workspace to confirm the deletion. <font color="red">The data contained in the workspace will be lost</font>.

### Use the workspace

When launching the "code-server" for a workspace, a new browser window is opened, containing a Visual Studio Code, running entirely within the workspace.

<img src="doc/images/code_server.png" alt="Code Server" width="800" height="auto">

From this window, it is possible to clone a repository and work on the files. (to access external repositories, check the next chapter)

### Use the Smithy CLI

There are two ways of accessing the shell of the workspace, where you can use the Smithy CLI (or other linux tools).
- In the coder interface for your workspace, click the "Terminal" button. A new browser window will open, within which you have a shell prompt.

<img src="doc/images/coder_terminal.png" alt="Coder Terminal" width="250" height="auto">

- Directly in Visual studio code, you can open a Terminal panel by going in the menu Terminal-> New terminal (or by using the shortcut).

<img src="doc/images/vscode_terminal.png" alt="VsCode Terminal" width="600" height="auto">

## Access Gitlab from a workspace (or a git client)

To access a Gitlab git reository, you need to use the specific url for that repo. When browsing the repo, it is presented in the "code" button.

<img src="doc/images/gitlab_repo_uri.png" alt="Gitlab Profile" width="250" height="auto">

Note that HTTPS is available on all Gitlab instances f DIGIT while SSH is available only on code.europa.eu. It is therefore recommended to get use to using HTTPS.


### Using HTTPS

You can interact with the Gitlab repo using HTTPS and user/password credentails.
Your standard user using EULogin and 2 factor authentication, it cannot by used to interact using a git client, so you need to create a specific "Token" (or password) that can be used together with your user to perform some specific activities.

Creating this token is done via your Gitlab profile:

<img src="doc/images/gitlab_profile.png" alt="Gitlab Profile" width="250" height="auto">
<img src="doc/images/gitlab_profile_token.png" alt="Gitlab Token" width="250" height="auto">
<img src="doc/images/gitlab_profile_add_token.png" alt="Gitlab Add Token" width="250" height="auto">

You can then give a name to the new token (interesting to remember what it is used at), an expiration date (mandatory) and the permissions linked to it.
To work on code, all you need is "read_repository" and "write_repository":

<img src="doc/images/gitlab_token_permissions.png" alt="Gitlab Token permissions" width="auto" height="auto">

When creating the token you will receive the created token that you can see (with the eye icon) or copy (with the clipboad button).  **Be careful.  You will not be able afterward to see the token anymore**, you have to write it down.
Later you will only be able to see the name of the token to see when it was used and delete it.

From that moment, you can use your **user** (not your email address) and this token from any git client to interact with the repository.

<img src="doc/images/gitlab_user.png" alt="Gitlab User" width="250" height="auto">


### Using SSH (deprecated)

Each coder user has an automatically generated SSH key pair, that is used by all of his workspaces when using ssh-based tools.

The public part of this keypair is available through the account menu in the section "SSH Keys"

<img src="doc/images/account.png" alt="Login" width="250" height="auto">
<img src="doc/images/ssh_key.png" alt="SSH key" width="600" height="auto">

This key can be used to get direct access to remote repositories like on Gitlab, by adding it to your profile.
Be aware that if you click on "Regenerate...", the previous key will be lost and you will have to update it in all remote locations where it had been registered.

In the case of Gitlab it is done by going to your profile, in the entry "SSH Keys" by selecting "Add New Key".

<img src="doc/images/gitlab_profile.png" alt="Gitlab Profile" width="250" height="auto">
<img src="doc/images/gitlab_ssh_menu.png" alt="Gitlab ssh_menu" width="250" height="auto">
<img src="doc/images/gitlab_add_ssh.png" alt="SSH Add key" width="200" height="auto">
<img src="doc/images/gitlab_add_ssh_form.png" alt="SSH key form" width="650" height="auto">

In the form, copy the key available in you Coder profile and paste in in the "Key" field.
Give it a title so you can remember where it comes from (to allow you to delete this key when it is not relevant anymore), and for Usage type, "Authentication" should be enough.

## Bootstrap your Smithy project

A [repository](https://code.europa.eu/digit-c4/smithy-tools/smithy-startup-template) has been created to propose the skeleton of a Smithy definition, that can be used to start working on a new service definition.

Please, do not push modifications to it, but do you own fork or a least on a separate branch from the main.
