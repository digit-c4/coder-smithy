---
name: Develop in Docker with Smithy
description: Develop inside Docker containers with smithy tools available
tags: [local, docker, smithy]
icon: doc/smithy.svg
---

![Smithy](doc/smithy.svg)

# Smithy the easy way

Provides you with a workspace and a web-based visual studio code allowing you to work on smithy files without installing additional dependencies.

Included tools are:
- Smithy CLI
- Smithy VSCode addon
- Smithy language server

